#!/bin/bash
#
# Start a Celery worker pool (processes by default) that serves the ewoks application.

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
( cd $SCRIPT_DIR; ewoksjob worker )