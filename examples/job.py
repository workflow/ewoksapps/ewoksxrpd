import os
import re
import json
import logging
import argparse
from typing import List

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
TOP_DIR = os.path.dirname(SCRIPT_DIR)
DATA_DIR = os.path.join(SCRIPT_DIR, "data")
RESULT_DIR = os.path.join(SCRIPT_DIR, "results")
TRANSIENT_DIR = os.path.join(SCRIPT_DIR, "transient")


def replace_path(path: str) -> str:
    """Replace absolute path"""
    if path.startswith("silx"):
        prefix, path, suffix = re.match(r"(silx\:\/\/)(.+)(examples.+)", path).groups()
        return f"{prefix}{os.path.join(TOP_DIR, suffix)}"
    else:
        path, suffix = re.match("(.+)(examples.+)", path).groups()
        return os.path.join(TOP_DIR, suffix)


def path_inputs(workflow: str) -> List[dict]:
    """Replace absolute paths"""
    inputs = []
    with open(workflow, "r") as f:
        data = json.load(f)

    for node_attrs in data["nodes"]:
        for arg in node_attrs.get("default_inputs", list()):
            value = arg["value"]
            if isinstance(value, str):
                if "examples" not in value:
                    continue
                value = replace_path(value)
            elif isinstance(value, list):
                if (
                    not value
                    or not isinstance(value[0], str)
                    or "examples" not in value[0]
                ):
                    continue
                value = [replace_path(v) for v in value]
            else:
                continue
            inputs.append(
                {
                    "id": arg["id"],
                    "name": arg["name"],
                    "value": value,
                }
            )
    return inputs


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Launch XRPD workflow")
    parser.add_argument(
        "--celery",
        dest="celery",
        action="store_true",
        help="Use celery worker",
    )
    options = parser.parse_args()

    if options.celery:
        from ewoksjob.client import submit

    else:
        from ewoks import execute_graph

        logging.basicConfig()
        logging.getLogger("ewokscore").setLevel(logging.INFO)

    workflow = os.path.join(SCRIPT_DIR, "xrpd_workflow.json")
    inputs = path_inputs(workflow)

    varinfo = {"root_uri": os.path.join(TRANSIENT_DIR, "result.nx"), "scheme": "nexus"}

    if options.celery:
        future = submit(
            workflow,
            varinfo=varinfo,
            inputs=inputs,
        )
        result = future.get()
    else:
        result = execute_graph(
            workflow,
            varinfo=varinfo,
            inputs=inputs,
        )
    print(result)
