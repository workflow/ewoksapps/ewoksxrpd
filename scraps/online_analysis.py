"""

Usage: online_analysis (-s <session_name> | --session=<session_name>) [-p <plugin_name> | --plugin=<plugin_name>]

Options:
    -s, --session=<session_name>  Start with the specified session
    -p, --plugin=<plugin_name>    Start with the specified plugin [default: Integrate]
"""

from docopt import docopt, DocoptExit
import gevent
from collections import defaultdict
import typing
import numpy

from bliss.scanning.scan_info import ScanInfo
from bliss.data.scan import ScansObserver, ScansWatcher
import bliss

import pyFAI
import pyFAI.units
import pyFAI.utils
import pyFAI.worker
from pyFAI.utils.shell import ProgressBar

hc = pyFAI.units.hc

CALC_OBJECT = None


class Integrate:
    def setup(self):
        # configure worker
        # ai = pyFAI.worker.make_ai({})
        # self.worker = pyFAI.worker.Worker(azimuthalIntegrator=ai)
        self.worker = pyFAI.worker.Worker()
        pass

    def load_config(self, info):
        self.config_worker = json.load(open(info))
        self.config_worker["method"] = "csr_ocl_gpu"
        self.worker.set_config(info, consume_keys=False)
        self.writer = pyFAI.io.DefaultAiWriter("", self.worker.ai)
        pass

    def calc(self, raw_data, info):
        print("here is raw_data", raw_data, info)
        # outfile_img =  # set the path of the file
        self.writer.set_filename(outfile_img)
        self.worker.process(
            raw_data, writer=self.writer, normalization_factor=info["scaled_mondio"]
        )


class ScanWithLimaDataObserver(ScansObserver):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ignore_scan_list = []
        self._processing_info = {}
        self._image_index = defaultdict(int)
        self._diode_values = defaultdict(list)
        CALC_OBJECT.setup()

    def on_scan_started(self, scan_db_name: str, scan_info: dict):
        has_lima_data = any(
            dev.get("type") == "lima" for dev in scan_info["devices"].values()
        )
        if has_lima_data:
            print(f"Scan started: {scan_db_name}")
            processing_info = scan_info.get("instrument").get("processing_info")
            if processing_info:
                self._processing_info[scan_db_name] = processing_info

                CALC_OBJECT.load_config(processing_info)

                return

        self._ignore_scan_list.append(scan_db_name)

    def on_scalar_data_received(
        self,
        scan_db_name: str,
        channel_name: str,
        index: int,
        data_bunch: typing.Union[list, numpy.ndarray],
    ):
        if scan_db_name in self._ignore_scan_list:
            return
        if channel_name.endswith("scaled_mondio"):
            self._diode_values[scan_db_name] += list(data_bunch)
            print("scaled_mondio data", self._diode_values)

    def on_lima_ref_received(
        self, scan_db_name: str, channel_name: str, dim: int, source_node, event_data
    ):
        if scan_db_name in self._ignore_scan_list:
            return
        processing_info = self._processing_info[scan_db_name]
        # print(event_data)
        data_view = source_node.get(
            self._image_index[scan_db_name], event_data.description["last_image_ready"]
        )
        for i in range(len(data_view)):
            self._image_index[scan_db_name] += 1
            image_index = self._image_index[scan_db_name]
            diode_values = self._diode_values[scan_db_name]
            if len(diode_values) < image_index:
                return
            processing_info["scaled_mondio"] = diode_values[image_index]
            raw_data = data_view.get_image(i)
            CALC_OBJECT.calc(raw_data, processing_info)

    def on_scan_finished(self, scan_db_name, *args):
        try:
            self._ignore_scan_list.remove(scan_db_name)
        except ValueError:
            # scan has not been ignored => maybe needs to remove processing info
            # and image indexes
            self._processing_info.pop(scan_db_name, None)
            self._image_index.pop(scan_db_name, None)
            self._diode_values.pop(scan_db_name, None)


def watch(session_name):
    watcher = ScansWatcher(session_name)
    watcher.set_exclude_existing_scans(True)
    observer = ScanWithLimaDataObserver()
    watcher.set_observer(observer)
    return watcher


if __name__ == "__main__":
    arguments = docopt(__doc__)
    session_name = arguments["--session"]
    plugin_name = arguments["--plugin"]

    print("ID31 Online Processing")
    CALC_OBJECT = globals()[plugin_name]()
    watcher = watch(session_name)
    watcher_task = gevent.spawn(watcher.run)
    watcher.wait_ready(timeout=3)
    print(
        f"Listening to BLISS session: {session_name}, will apply plugin {repr(plugin_name)} on 2D data"
    )
    watcher_task.join()
