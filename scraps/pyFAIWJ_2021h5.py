#!/usr/bin/env python3
# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from PyQt5.QtCore import QObject
import logging
import sys
import tempfile
import os

import numpy
import six
import pdb

logging.basicConfig()
_logger = logging.getLogger("hdf5widget")
"""Module logger"""

import silx.gui.hdf5
import silx.utils.html
from silx.gui import qt
from silx.gui.data.DataViewerFrame import DataViewerFrame
from silx.gui.widgets.ThreadPoolPushButton import ThreadPoolPushButton

import json
import time
import fabio
import pyFAI
import pyFAI.units
import pyFAI.utils
import pyFAI.worker
from pyFAI.utils.shell import ProgressBar
import h5py
import hdf5plugin

hc = pyFAI.units.hc
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("PyFAI")

try:
    from argparse import ArgumentParser
except ImportError:
    from pyFAI.third_party.argparse import ArgumentParser

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"

# norm=1e-10
norm = 1
monitors = ["scaled_mondio", "mondio", "srcur"]
transmission = "detdio"
# method = "csr"
method = "csr_ocl_gpu"
ext = ".dat"
tomoth = "instrument/positioners/tomoth"
tomoy = "instrument/saby/data"
_file_cache = {}


class Hdf5TreeView(qt.QMainWindow):
    """
    This window show an example of use of a Hdf5TreeView.

    The tree is initialized with a list of filenames. A panel allow to play
    with internal property configuration of the widget, and a text screen
    allow to display events.
    """

    def __init__(self, filenames=None, options=None):
        """
        :param files_: List of HDF5 or Spec files (pathes or
            :class:`silx.io.spech5.SpecH5` or :class:`h5py.File`
            instances)
        """
        qt.QMainWindow.__init__(self)
        self.setWindowTitle("ID31 integration")

        self.__asyncload = True
        self.__treeview = silx.gui.hdf5.Hdf5TreeView(self)

        main_panel = qt.QWidget(self)

        layout = qt.QVBoxLayout()
        layout.addWidget(self.__treeview, 1)

        self.options = options
        layout.addWidget(self.createTreeViewConfigurationPanel(self, self.__treeview))
        main_panel.setLayout(layout)

        self.setCentralWidget(main_panel)

        self.resize(800, 800)

        self.__treeview.setSelectionMode(qt.QAbstractItemView.MultiSelection)

        # append all files to the tree
        self.model = self.__treeview.findHdf5TreeModel()
        # print(self.model)
        for file_name in filenames:
            self.model.insertFile(file_name)

        """   
            row = self.model.h5pyObjectRow(h5)
            index = self.model.index(row, 0, qt.QModelIndex())
            self.__treeview.setExpanded(index, True)
        """

        self.__treeview.activated.connect(self.displayData)
        self.monitor = monitors[0]

    def displayData(self):
        """Called to update the dataviewer with the selected data."""
        selected = list(self.__treeview.selectedH5Nodes())

    # print(selected)

    def __fileCreated(self, filename):
        if self.__asyncload:
            self.__treeview.findHdf5TreeModel().insertFileAsync(filename)
        else:
            self.__treeview.findHdf5TreeModel().insertFile(filename)

    def __hdf5ComboChanged(self, index):
        function = self.__hdf5Combo.itemData(index)
        self.__createHdf5Button.setCallable(function)

    def __onOpenFileButtonClickedJSON(self):
        filename, filt = qt.QFileDialog.getOpenFileName(
            parent=self, caption="Open file", filter="*.json"
        )

        print(filename)

        if filename:
            self.textbox_json.setText(filename)
            self.options.jsonfile = filename

    def __onTextboxEditedJSON(self):
        self.options.jsonfile = self.textbox_json.text()

    def __onOpenFileButtonClickedOut(self):
        foldername = qt.QFileDialog.getExistingDirectory(
            parent=self, caption="Select Folder"
        )

        if foldername:
            self.textbox_out.setText(foldername)
            self.options.out = foldername

    def __onTextboxEditedOut(self):
        self.options.out = self.textbox_out.text()

    def __refreshSelected(self):
        """Refresh all selected items"""
        qt.QApplication.setOverrideCursor(qt.Qt.WaitCursor)

        selection = self.__treeview.selectionModel()
        indexes = selection.selectedIndexes()
        selectedItems = []
        model = self.__treeview.model()
        h5files = set([])
        while len(indexes) > 0:
            index = indexes.pop(0)
            # print(model.data(index))

            if ".h5" in str(model.data(index)):  # is the index root file?
                h5 = model.data(
                    index, role=silx.gui.hdf5.Hdf5TreeModel.H5PY_OBJECT_ROLE
                )

                h5files.add(h5.file)

            """
            rootIndex = index
            # Reach the root of the tree
            while rootIndex.parent().isValid():
                rootIndex = rootIndex.parent()

            if rootIndex == index:
                h5 = model.data(index, role=silx.gui.hdf5.Hdf5TreeModel.H5PY_OBJECT_ROLE)
                h5files.add(h5.file)
            print(len(h5files))
            """

        if len(h5files) == 0:
            qt.QApplication.restoreOverrideCursor()
            return

        # print('h5files: ',h5files)
        model = self.__treeview.findHdf5TreeModel()
        for h5 in h5files:
            self.__synchronizeH5pyObject(h5, model)
        #    model.synchronizeH5pyObject(h5)
        """
        model = self.__treeview.model()
        itemSelection = qt.QItemSelection()
        for rootRow, relativePath in selectedItems:
            rootIndex = model.index(rootRow, 0, qt.QModelIndex())
            index = self.__indexFromPath(model, rootIndex, relativePath)
            if index is None:
                continue
            indexEnd = model.index(index.row(), model.columnCount() - 1, index.parent())
            itemSelection.select(index, indexEnd)
        selection.select(itemSelection, qt.QItemSelectionModel.ClearAndSelect)
        """
        qt.QApplication.restoreOverrideCursor()

    def __synchronizeH5pyObject(self, h5, model):
        # model = self.__treeview.findHdf5TreeModel()
        # This is buggy right now while h5py do not allow to close a file
        # while references are still used.
        # FIXME: The architecture have to be reworked to support this feature.
        # model.synchronizeH5pyObject(h5)

        filename = h5.filename
        row = model.h5pyObjectRow(h5)
        index = self.__treeview.model().index(row, 0, qt.QModelIndex())
        # paths = self.__getPathFromExpandedNodes(self.__treeview, index)
        # model.removeH5pyObject(h5)
        model.clear()
        model.insertFile(filename, row)
        # index = self.__treeview.model().index(row, 0, qt.QModelIndex())
        # self.__treeview.setExpanded(index, True)
        # self.__expandNodesFromPaths(self.__treeview, index, paths)

    def __expandNodesFromPaths(self, view, rootIndex, paths):
        model = view.model()
        for path in paths:
            index = self.__indexFromPath(model, rootIndex, path)
            if index is not None:
                view.setExpanded(index, True)

    def __getPathFromExpandedNodes(self, view, rootIndex):
        """Return relative path from the root index of the extended nodes"""
        model = view.model()
        rootPath = None
        paths = []
        indexes = [rootIndex]
        while len(indexes):
            index = indexes.pop(0)
            if not view.isExpanded(index):
                continue

            node = model.data(index, role=silx.gui.hdf5.Hdf5TreeModel.H5PY_ITEM_ROLE)
            path = node._getCanonicalName()
            if rootPath is None:
                rootPath = path
            path = path[len(rootPath) :]
            paths.append(path)

            for child in range(model.rowCount(index)):
                childIndex = model.index(child, 0, index)
                indexes.append(childIndex)
        return paths

    def __getRelativePath(self, model, rootIndex, index):
        """Returns a relative path from an index to his rootIndex.
        If the path is empty the index is also the rootIndex.
        """
        path = ""
        while index.isValid():
            if index == rootIndex:
                return path
            name = model.data(index)
            if path == "":
                path = name
            else:
                path = name + "/" + path
            index = index.parent()

        # index is not a children of rootIndex
        raise ValueError("index is not a children of the rootIndex")

    def __indexFromPath(self, model, rootIndex, path):
        elements = path.split("/")
        if elements[0] == "":
            elements.pop(0)
        index = rootIndex
        while len(elements) != 0:
            element = elements.pop(0)
            found = False
            for child in range(model.rowCount(index)):
                childIndex = model.index(child, 0, index)
                name = model.data(childIndex)
                if element == name:
                    index = childIndex
                    found = True
                    break
            if not found:
                return None
        return

    def mon_select_change(self, i):
        self.monitor = monitors[i]

    def createTreeViewConfigurationPanel(self, parent, treeview):
        """Create a configuration panel to allow to play with widget states"""
        panel = qt.QWidget(parent)
        panel.setLayout(qt.QVBoxLayout())

        self.export_edf = qt.QCheckBox("Export as .edf")
        self.tomo = qt.QCheckBox("Tomography")
        self.p3 = qt.QCheckBox("p3")
        self.p3.setChecked(True)
        self.p3.stateChanged.connect(self.__unchecked)
        self.de = qt.QCheckBox("de")
        self.de.stateChanged.connect(self.__unchecked)

        hbox = qt.QHBoxLayout()

        hbox.addWidget(self.p3)
        hbox.addWidget(self.de)
        hbox.addWidget(self.export_edf)
        hbox.addWidget(self.tomo)

        panel.layout().addLayout(hbox)

        content2 = qt.QGroupBox("JSON file", panel)
        content2.setLayout(qt.QHBoxLayout())
        panel.layout().addWidget(content2)
        self.textbox_json = qt.QLineEdit(content2)
        self.textbox_json.insert(self.options.jsonfile)
        content2.layout().addWidget(self.textbox_json)
        self.button_json = qt.QPushButton("...")
        content2.layout().addWidget(self.button_json)
        self.button_json.clicked.connect(self.__onOpenFileButtonClickedJSON)
        self.textbox_json.returnPressed.connect(self.__onTextboxEditedJSON)

        content3 = qt.QGroupBox("Output folder", panel)
        content3.setLayout(qt.QHBoxLayout())
        panel.layout().addWidget(content3)
        self.textbox_out = qt.QLineEdit(content3)
        self.textbox_out.insert(self.options.out)
        content3.layout().addWidget(self.textbox_out)
        self.button_out = qt.QPushButton("...")
        content3.layout().addWidget(self.button_out)
        self.button_out.clicked.connect(self.__onOpenFileButtonClickedOut)
        self.textbox_out.textEdited.connect(self.__onTextboxEditedOut)

        self.l1 = qt.QLabel()
        self.l1.setText("Monitor:")
        self.cb1 = qt.QComboBox()
        self.cb1.addItems(monitors)
        self.cb1.currentIndexChanged.connect(self.mon_select_change)
        self.b1 = ThreadPoolPushButton(panel, text="Reload Selected HDF5")
        self.b1.setCallable(self.__refreshSelected)
        button = ThreadPoolPushButton(panel, text="Integrate")
        button.setCallable(self.__integrateAll)
        # button.succeeded.connect(self.__fileCreated)

        hbox1 = qt.QHBoxLayout()

        hbox1.addWidget(self.l1)
        hbox1.addWidget(self.cb1)
        hbox1.addWidget(self.b1)
        hbox1.addWidget(button)

        panel.layout().addLayout(hbox1)

        """
        content4 = qt.QGroupBox("Output", panel)
        content4.setLayout(qt.QHBoxLayout())
        self.textEdit = qt.QTextEdit(content4)
        self.textEdit.setReadOnly(True)
      #  self.cursor = qt.QTextCursor(self.textEdit.document())        
        content4.layout().addWidget(self.textEdit)
        panel.layout().addWidget(content4)
        """

        # panel.layout().addStretch(1)

        return panel

    def __unchecked(self, state):
        if state == 2:
            # if first check box is selected
            if self.sender() == self.p3:
                # making other check box to uncheck
                self.de.setChecked(False)

            # if second check box is selected
            elif self.sender() == self.de:
                # making other check box to uncheck
                self.p3.setChecked(False)

    def getDEdata(self, dirname, scanno):
        scanname = "scan" + "{:04d}".format(scanno)
        imgspath = os.path.join(dirname, scanname)
        imgs = [i for i in os.listdir(imgspath) if "de" in i]
        imgs.sort()
        if "data" in locals():
            del data
        data = [None] * len(imgs)
        for i, img in enumerate(imgs):
            print(img)
            with h5py.File(os.path.join(imgspath, img), "r") as h5:
                data[i] = h5["entry_0000"]["ESRF-ID31"]["de"]["data"][:][0]
        return numpy.array(data, dtype="int32")

    def __integrateAll(self):
        if self.options.jsonfile:
            # set the config
            self.config_worker = json.load(open(self.options.jsonfile))
            self.config_worker["method"] = method

            # set worker
            ai = pyFAI.worker.make_ai(self.config_worker)
            self.worker = pyFAI.worker.Worker(azimuthalIntegrator=ai)

            self.worker.set_config(self.config_worker, consume_keys=False)

            #  print(self.config_worker)

            print(
                "Mask: %s\tMethod: %s"
                % (self.config_worker["mask_file"], self.config_worker["method"])
            )

            if self.tomo.isChecked() == True:
                self.__integrateTomo()
            else:
                self.__integrateScans()

        else:
            print("No json file or no files chosen, no integration.")

    def __integrateScans(self):
        # set writer
        self.writer = pyFAI.io.DefaultAiWriter("", self.worker.ai)

        for h5_obj in self.__treeview.selectedH5Nodes():
            if h5_obj.attrs.get("NX_class") == "NXentry" and h5_obj.name[-1] == "1":
                start_time = time.time()
                dirname = os.path.dirname(os.path.abspath(h5_obj.file.filename))
                filename = os.path.basename(h5_obj.file.filename)
                scanno = int(h5_obj.name[1:-2])

                if self.p3.isChecked() == True:
                    imgdata = h5_obj.get("measurement/p3")
                if self.de.isChecked() == True:
                    # imgdata = h5_obj.get('measurement/de')
                    imgdata = self.getDEdata(dirname, scanno)

                if len(imgdata.shape) == 2:
                    imgdata = numpy.expand_dims(imgdata, axis=0)
                # print(len(imgdata))

                if (
                    self.monitor in h5_obj.get("measurement").keys()
                ):  # mondio in stream 1
                    mondio = numpy.atleast_1d(
                        h5_obj.get("measurement/" + self.monitor)[()]
                    )
                else:
                    mondio = numpy.ones(len(imgdata))

                if self.options.out:
                    outfile_base = os.path.join(
                        self.options.out, filename[:-3] + "_{:0>4d}".format(scanno)
                    )
                else:
                    outfile_base = os.path.join(
                        dirname, filename[:-3] + "_{:0>4d}".format(scanno)
                    )

                # self.textEdit.append('Outfile base: %s' % outfile_base)
                progress_bar = ProgressBar(
                    "Integrating scan number %s" % str(scanno), len(mondio), 20
                )

                for i, img in enumerate(imgdata):
                    progress_bar.update(i + 1, message="Frame %i" % i)
                    # integration
                    outfile_img = outfile_base + "_" + "{:0>4d}".format(i) + ext

                    # export to edf
                    edf_header = {}
                    if self.export_edf.isChecked() == True:
                        outfile_img_edf = (
                            outfile_base + "_" + "{:0>4d}".format(i) + ".edf"
                        )
                        edf_header["monitor"] = mondio[i]
                        edf = fabio.edfimage.edfimage(header=edf_header, data=img)
                        edf.write(outfile_img_edf)

                    # set the writer
                    self.writer.set_filename(outfile_img)

                    start1_time = time.time()

                    self.worker.process(
                        img, writer=self.writer, normalization_factor=mondio[i] / norm
                    )

                    # print((time.time() - start1_time))

                progress_bar.clear()
                print(
                    "Processing done of scan %s done in %.3fs !"
                    % (str(scanno), (time.time() - start_time))
                )

        self.writer.close()

    def __integrateTomo(self):
        for h5_obj in self.__treeview.selectedH5Nodes():
            if h5_obj.attrs.get("NX_class") == "NXentry" and h5_obj.name[-1] == "1":
                start_time = time.time()
                dirname = os.path.dirname(os.path.abspath(h5_obj.file.filename))
                filename = os.path.basename(h5_obj.file.filename)
                scanno = int(h5_obj.name[1:-2])

                outfile = os.path.join(
                    self.options.out, filename[:-3] + "_{:0>4d}".format(scanno) + ".h5"
                )
                f = h5py.File(outfile, "w")

                # get the images + epoch times
                imgdata = h5_obj.get("measurement/p3")
                img_epoch = h5_obj.get("measurement/epoch_trig")

                if (
                    self.monitor in h5_obj.get("measurement").keys()
                ):  # mondio in stream 1
                    mondio = h5_obj.get("measurement/" + self.monitor)
                else:  # mondio in stream 2
                    # get the mondio readings
                    h5_parent = h5_obj.local_file
                    key = list(
                        filter(lambda x: h5_obj.basename in x, h5_parent.keys())
                    )  # find the key of the object
                    h5_mon_name = key[0][:-1] + "2"
                    mon_epoch = h5_parent[h5_mon_name + "/measurement/epoch"]
                    mon_mon = h5_parent[h5_mon_name + "/measurement/" + self.monitor]

                    # interpolate mondio readings
                    mondio = numpy.interp(img_epoch, mon_epoch, mon_mon)

                if len(imgdata.shape) == 2:
                    imgdata = numpy.expand_dims(imgdata, axis=0)

                progress_bar = ProgressBar(
                    "Integrating scan number %s" % str(scanno), len(mondio), 20
                )

                th = h5_obj.get(tomoth)
                f.create_dataset("th", data=th[:])
                y = h5_obj.get(tomoy)
                f.create_dataset("y", data=y[:])
                xrd = f.create_dataset(
                    "XRD", (len(imgdata), self.config_worker["nbpt_rad"])
                )

                for i, img in enumerate(imgdata):
                    progress_bar.update(i + 1, message="Frame %i" % i)
                    # integration
                    res = self.worker.process(
                        img, normalization_factor=mondio[i] / norm
                    )
                    if i == 0:
                        f.create_dataset("q", data=res[:, 0])
                    xrd[i, :] = res[:, 1]

                f.close()
                progress_bar.clear()
                print(
                    "Processing done of scan %s done in %.3fs !"
                    % (str(scanno), (time.time() - start_time))
                )


def main():
    usage = "pyFAIWJ_2021 [options] file1.h5 file2.h5 ..."
    version = "pyFAI version %s from %s" % (pyFAI.version, pyFAI.date)
    description = "Azimuthal integration for powder diffraction for ESRF ID31 beamline."
    epilog = """    """
    parser = ArgumentParser(usage=usage, description=description, epilog=epilog)
    parser.add_argument("-v", "--version", action="version", version=version)
    parser.add_argument(
        "-j",
        "--json",
        dest="jsonfile",
        type=str,
        default=None,
        help="JSON file with the integration parameters",
    )
    parser.add_argument(
        "-o", "--out", dest="out", type=str, default=None, help="Output folder"
    )

    parser.add_argument(
        "args", metavar="FILE", type=str, nargs="*", help="Files to be integrated"
    )

    options = parser.parse_args()

    if options.jsonfile == None:
        print("No jsonfile option")
    else:
        options.jsonfile = os.path.abspath(options.jsonfile)
    if options.out == None:
        print("No output directory option")
    else:
        options.out = os.path.abspath(options.out)

    filenames = pyFAI.utils.expand_args(options.args)

    app = qt.QApplication([])
    sys.excepthook = qt.exceptionHandler
    window = Hdf5TreeView(filenames, options)
    window.show()
    result = app.exec_()
    # remove ending warnings relative to QTimer
    app.deleteLater()
    sys.exit(result)


if __name__ == "__main__":
    main()
