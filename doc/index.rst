ewoksxrpd |version|
===================

*ewoksxrpd* provides data processing workflows for SAXS/WAXS.

*ewoksxrpd* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.eu/>`_.

How to install
--------------

Install the `ewoksxrpd <https://ewoksxrpd.readthedocs.io/en/latest/>`_ Python package

.. code:: bash

    pip install ewoksxrpd

How to use
-----------

Run an example workflow

.. code:: bash

    python examples/job.py

Run an example workflow with GUI

.. code:: bash

    ewoks execute examples/xrpd_workflow.json --engine=orange --data-root-uri=/tmp --data-scheme nexus

or for an installation with the system python

.. code:: bash

    python3 -m ewoks execute examples/xrpd_workflow.json --engine=orange --data-root-uri=/tmp --data-scheme nexus

Produce the example data

.. code:: bash

    pytest --examples

Documentation
-------------

.. toctree::
    :maxdepth: 2

    pyfai
    api