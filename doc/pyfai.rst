pyFAI tasks
===========

Integration options
-------------------

The `PyFaiConfig` task has a parameter called `integration_options` which is a dictionary
of parameters expected by a pyFAI worker on `configuration <https://pyfai.readthedocs.io/en/stable/api/pyFAI.html#pyFAI.worker.Worker.set_config>`_
or `instantiation <https://pyfai.readthedocs.io/en/stable/api/pyFAI.html#pyFAI.worker.Worker.__init__>`_.

pyFAI itself stores these options in the `pyfai-integrate <https://pyfai.readthedocs.io/en/stable/man/pyFAI-integrate.html>`_ application configuration files (JSON) or
`PONI <https://pyfai.readthedocs.io/en/stable/api/io/index.html?highlight=ponifile#pyFAI.io.ponifile.PoniFile>`_ files (only the detector and beam parameters).
Most often you will provide the `integration_options` through one of those parameter files. Below we describe the most important parameters.

Detector
++++++++

The detector `position <https://pyfai.readthedocs.io/en/stable/geometry.html#detector-position>`_ parameters

* `dist (m)`: perpendicular sample-detector distance
* `poni1 (m)`: first coordinate of the point of normal incidence
* `poni2 (m)`: first coordinate of the point of normal incidence
* `rot1 (rad)`: detector orientation
* `rot2 (rad)`: detector orientation
* `rot3 (rad)`: detector orientation

The detector `type <https://pyfai.readthedocs.io/en/stable/detector.html>`_ parameters

* `detector`: name of a registered detector
* `detector_config`: registered detector instantiation options
* `pixel1 (m)`: pixel size (detector instantiation option)
* `pixel2 (m)`: pixel size (detector instantiation option)
* `splineFile`: path to file containing the geometric correction (detector instantiation option)
* `mask_file`: path to file containing bad pixels (pixels with 0 value are not masked)

Primary beam
++++++++++++

* `wavelength (m)`: monochromatic beam
* `polarization_factor`: from `-1` (vertical) to `+1` (horizontal) for linear polarization (synchrotrons are around 0.95) and `None` for unpolarized beam

Integration grid
++++++++++++++++

For azimuthal integration and unwrapping

* `nbpt_rad`: number of points in the radial direction
* `radial_range_min`: radial range
* `radial_range_max`: radial range

For unwrapping only (a.k.a. caking)

* `nbpt_azim`: number of points in the azimuthal direction
* `azimuth_range_min`: azimuthal range
* `azimuth_range_max`: azimuthal range

Integration method
++++++++++++++++++

* `method`: a string with the format `"{splitting}_{algorithm}_{implementation}[_{target}]"`

  * `splitting <https://pyfai.readthedocs.io/en/stable/design/ai.html?highlight=splitting#pixel-splitting>`_: no, bbox, pseudo, full
  * `algorithm <https://pyfai.readthedocs.io/en/stable/design/ai.html?highlight=splitting#rebinning-engines>`_: csr, csc, lut, histogram
  * implementation: python, cython, ocl
  * target: `gpu` (autoselect GPU device) or `0,1` (device 1 of platform 0)
* `integrator_name`:

  * unwrapping

    * `integrate2d_ng`: coordinate transformation from X-Y to Radius-Azimuth
    * `integrate2d_legacy`:
    * `medfilt1d`: `integrate2d_ng` with median filtering in both directions
  * azimuthal integration

    * `integrate1d_ng`: azimuthal integrated profile as a function of radius (2-theta, Q, ...)
    * `integrate1d_legacy`:
    * `sigma_clip_ng`: with outlier rejection (does not support pixel splitting)
    * `sigma_clip_legacy`: with outlier rejection (does support pixel splitting)
  * radial integration

    * `integrate_radial`: radial integrated profile as a function of azimuth

Extra options
+++++++++++++

Different methods have extra options which should be provided by an `extra` dictionary with these keys

* `max_iter` (`sigma_clip_ng` and `sigma_clip_legacy`): number of rejections cycles
* `thres` (`sigma_clip_ng` and `sigma_clip_legacy`): percentile for the rejection in each cycle
* `npt_azim` (`sigma_clip_legacy`): the `sigma_clip_legacy` method first does unwrapping and then azimuthal integration with rejection
