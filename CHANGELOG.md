# CHANGELOG.md

## Unreleased

## 1.0.0

Changes:

- Support new module name `pyFAI.integrator.azimuthal` deprecating the previous name
  `pyFAI.azimuthalIntegrator` since pyFAI version `2025.1.0`.
- Handle pyFAI version `2025.1.0` configuration changes.

## 0.15.0

New Ewoks tasks:

- `SaveNexusPatternsAsAscii`: convert azimuthal integration results from NeXus to ASCII files.
- `SavePyFaiConfig`: save pyFAI calibration and integration configuration file.
- `SavePyFaiPoniFile`: save pyFAI calibration file (PONI).

Changes:

- Drop support for python 3.7
- Require `pyfai >= 2024.1`
- PyFai integration configuration file v4 support.

Bug fixes:

- Fix incremental calibration parameter releasing during pyFAI calibration.
- Fix worker `nbpt_rad` and `nbpt_azim` default values.

## 0.14.0

⚠️ `Integrate1D` and `MultiConfigIntegrate1D` are deprecated and should be respectively replaced by `IntegrateSinglePattern` and `MultiConfigIntegrateSinglePattern`. For 1D integration purposes, the new tasks do the same job but have different output names:

| `(MultiConfig)Integrate1D` (deprecated) | `(MultiConfig)IntegrateSinglePattern` (new) |
| --------------------------------------- | ------------------------------------------- |
| `x`                                     | `radial`                                    |
| `y`                                     | `intensity`                                 |
| `xunits`                                | `radial_units`                              |
| `yerror`                                | `intensity_error`                           |

New features:

- `CalibrateSingle` now accepts a list of values for `max_rings` for sequential refinements.
- New task `IntegrateSinglePattern`: replacement of `Integrate1D`. Can perform 2D integration.
- New task `MultiConfigIntegrateSinglePattern`: replacement of `MultiConfigIntegrate1D`. Can perform 2D integration.

Bug fixes:

- pyFAI `Orientation` field is now properly serialized in the `info` output of integration tasks.

## 0.13.0

Changes:

- Support `blissdata` version API change

## 0.12.0

New features:

- Support `pyfai-integrate` application configuration format version 4

## 0.11.0

New features:

- New task `SaveAsciiMultiPattern1D` to save multiple 1D patterns in ASCII format

Bug fixes:

- `IntegrateBlissScan` and NeXus saving tasks: wait until the file has finished writing before creating links

## 0.10.1

Changes:

- integration tasks: add read, write and processing time to logs

Bug fixes:

- `Integrate1D`, `Integrate1DList`: add missing `flush_period` parameter
- `Integrate1DList`: only one `reference`
- `MultiConfigIntegrate1D`: use the merge pyFAI configurations
- data access with context iterators

## 0.10.0

New features:

- New task `MultiConfigIntegrate1D` to launch azimuthal integrations of an image with different configurations
- New task `SaveNexusMultiPattern1D` to save multiple 1D patterns in one NeXus file

Changes:

- Fix broken links in the documentation

## 0.9.0

New features:

- Add support for custom Lima file templates.

## 0.8.2

Bug fixes:

- Fix the `requires` section in `pyproject.toml`.

## 0.8.1

Changes:

- Make relative links to Bliss data when saving integration results.

## 0.8.0

New features:

- Add optional input `entry_name` to `Integrate1DList` (to modify the default "processing" if needed).
- Add optional input `ring_detector` to `CalibrateSingle` if the ring detection needs to be done on a different
  detector configuration than the one used for calibration.

Changes:

- Add column names to the ASCII output.
- Refactor integration tasks to clarify HDF5 master files vs. result files.
- Refactor calibration parameter releasing.

Bug fixes:

- Integration task image progress was off by 1.

## 0.7.0

New features:

- Add support for `pyFAI 2024.1`
- Add `monitor_uri ` to the output of `SumBlissScanImages` and `SumImages` tasks
- Improve error messages when no data is processed

Bug fixes:

- Fix project URLs in package metadata
- Handle relative file paths in `IntegrateBlissScan` and `SumBlissScanImages`
- Allow missing energy/wavelength in `PyFaiConfig` task

## 0.6.1

Bug fixes:

- remove print during integration

## 0.6.0

New features:

- Add `Integrate1DList`: task to integrate a list of images
- Add `SumBlissScanImages`: add diffraction patterns
- Add custom dark/flat correction (both raw counts)

## 0.5.0

New features:

- Support dark and flat from previous tasks

## 0.4.0

New features:

- `IntegrateBlissScan`: optionally use a master file (not open during processing) and a data file (open during processing)
- Integration tasks: argument to supply monitor and reference values
- Add new task to sum images `SumImages`
- Add new task to save images to EDF/TIFF `SaveImages`

Bug fixes:

- Handle numpy string when parsing units
- `IntegrateBlissScan`: Link to raw data are now in the master file

## 0.3.0

New features:

- Lima data from memory

## 0.2.0

New features:

- Azimuthal integration of a bliss scan without saving
- Error model support
- HDF5 saving: NXprocess as default is optional
- Integration tasks: additional parameter for integration options,
  to be used to overwrite the options coming from
  the configuration task
- Diagnostic tasks: support square-root scale for 1D plots
- Support pyFAI's "detector_config"
- Integration tasks: argument to change the worker pool size

Bug fixes:

- respect integration option "do_mask"

## 0.1.0

New features:

- Single and multi distance calibration
- Azimuthal integration of a single image
- Azimuthal integration of a bliss scan (during or after acquisition)
- Background subtraction
- Mask detection
- Save azimuthal integration results in HDF5
- Save azimuthal integration results in ASCII
- Azimuthal integration worker persistent in
  the process for each configuration
