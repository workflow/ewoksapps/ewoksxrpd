"""pytest command line arguments (not distributed)"""

import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--examples", action="store_true", default=False, help="generate example data"
    )


@pytest.fixture(scope="session")
def generate_examples(request) -> bool:
    return request.config.option.examples
